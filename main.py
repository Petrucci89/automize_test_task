from utils import JsonFileParser, GCPResourcesGraphBuilder, JsonGCPApiParser


assets_iam_parser = JsonFileParser.from_file('assets_iam', 'assets_iam_1066060271767_1578227771.1379685.json')
gcp_api_parser = JsonGCPApiParser.from_gcp_api('gcp_api', 'authomize-15054614561264767466-d96957db8913.json', 'ron@test.authomize.com')

gcp_builder = GCPResourcesGraphBuilder(assets_iam_parser, gcp_api_parser)

gcp_graph = gcp_builder.build()

print('======== TASK 1 ========\n', gcp_graph._nodes)

node = gcp_graph.get_node('resource', 'folders/188906894377')
hierarchy_nodes = []
gcp_graph.get_ancestors(node, 'hierarchy', hierarchy_nodes)
print('======== TASK 2 ========\n', hierarchy_nodes)

ident_node = gcp_graph.get_node('identity', 'ron@test.authomize.com')
who_has_what_result = gcp_graph.who_has_what(ident_node)
print('======== TASK 3 ========\n', who_has_what_result)

res_node = gcp_graph.get_node('resource', 'folders/635215680011')
what_has_who_result = gcp_graph.what_has_who(res_node)
print('======== TASK 4 ========\n', what_has_who_result)
