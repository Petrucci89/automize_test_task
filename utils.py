from typing import List
import json
from collections import namedtuple
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


class BaseStaticDataParser:
    def __init__(self, parse_tag):
        self.parse_tag = parse_tag

    @staticmethod
    def to_dict(obj):
        return obj

    def get_parsed_objects(self):
        return [self.to_dict(obj) for obj in self._get_objects()]
        # for obj in self._objects:
        #     yield self.to_dict(obj)


class JSONMixing:
    @staticmethod
    def to_dict(obj):
        return json.loads(obj)


class FileImportMixing:

    def _get_objects(self):
        with open(self._filename, 'r') as f:
            return f.readlines()

    @classmethod
    def from_file(cls, parse_tag, filename):
        i = cls(parse_tag)
        i._filename = filename
        return i


class GCPApiImportMixing:

    SCOPES = ['https://www.googleapis.com/auth/admin.directory.user.readonly',
              'https://www.googleapis.com/auth/admin.directory.group.readonly',
              'https://www.googleapis.com/auth/admin.directory.group.member.readonly']

    def _get_objects(self):
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            filename=self._keyfile_name,
            scopes=self.SCOPES
        )

        credentials = credentials.create_delegated(self._delegated_email)
        service = build('admin', 'directory_v1', credentials=credentials)

        results = service.groups().list(customer='my_customer', maxResults=50).execute()
        groups = results.get('groups', [])

        res = []

        for group in groups:
            group['members'] = list()

            results = service.members().list(groupKey=group['id']).execute()
            members = results.get('members', [])
            for member in members:
                group['members'].append(member)

            res.append(group)

        return res

    @classmethod
    def from_gcp_api(cls, parse_tag, keyfile_name, delegated_email):
        i = cls(parse_tag)
        i._keyfile_name = keyfile_name
        i._delegated_email = delegated_email
        return i


class JsonFileParser(FileImportMixing, JSONMixing, BaseStaticDataParser):
    pass


class JsonGCPApiParser(GCPApiImportMixing, BaseStaticDataParser):
    pass


IN_OUT = namedtuple('IN_OUT', ['inbound', 'outbound'])


class Node:
    def __init__(self, t, id, st=None):
        self._type = t  # 'identity', 'resource', ...
        self._id = id
        self._subtype = st
        self._relations = dict()

    def add_inbound(self, relation, node):
        try:
            if node not in self._relations[relation].inbound:
                self._relations[relation].inbound.append(node)
        except KeyError:
            self._relations[relation] = IN_OUT([node], [])

    def add_outbound(self, relation, node):
        try:
            if node not in self._relations[relation].outbound:
                self._relations[relation].outbound.append(node)
        except KeyError:
            self._relations[relation] = IN_OUT([], [node])

    def get_inbounds(self, relation):
        try:
            return self._relations[relation].inbound
        except KeyError:
            return []

    def get_outbounds(self, relation):
        try:
            return self._relations[relation].outbound
        except KeyError:
            return []

    def __eq__(self, other):
        if not isinstance(other, Node):
            return NotImplemented
        return self._type == other._type and self._id == other._id

    def __repr__(self):
        return f'{self._subtype} {self._type}: {self._id}'


class BaseGraphBuilder:
    def __init__(self, *parsers):
        self._parsers = parsers
        self._nodes = []
        self._map = dict()

    def find_node(self, t, id):
        try:
            return self._map[(t, id)]
        except:
            for n in self._nodes:
                if (n._type, n._id) == (t, id):
                    return n

    def add_node(self, t, id, st):
        node = self.find_node(t, id)
        if not node:
            node = Node(t, id, st)
            self._map[(t, id)] = node
            self._nodes.append(node)
        return node

    def _represent(self, objects):
        pass

    def _collect_graph(self):
        for parser in self._parsers:
            self._represent(parser)

    def build(self):
        self._collect_graph()
        return GCPGraph(self._nodes, self._map)


class GCPResourcesGraphBuilder(BaseGraphBuilder):

    def _represent(self, parser):

        if parser.parse_tag == 'assets_iam':

            for obj in parser.get_parsed_objects():
                res_subtype = obj['asset_type'].split('/')[-1]
                res_node = self.add_node('resource', obj['name'].split('googleapis.com/')[1], res_subtype)

                for binding in obj['iam_policy']['bindings']:
                    for member in binding['members']:
                        ident_node = self.add_node('identity', member.split(':')[1], member.split(':')[0].title())
                        ident_node.add_outbound(binding['role'], res_node)
                        res_node.add_inbound(binding['role'], ident_node)

            for obj in parser.get_parsed_objects():
                if len(obj['ancestors']) > 1:
                    for i in range(len(obj['ancestors']) - 1):
                        node = self.find_node('resource', obj['ancestors'][i])
                        parent_node = self.find_node('resource', obj['ancestors'][i+1])
                        node.add_inbound('hierarchy', parent_node)
                        parent_node.add_outbound('hierarchy', node)

        if parser.parse_tag == 'gcp_api':
            for group in parser.get_parsed_objects():
                group_node = self.add_node('identity', group['email'], 'Group')
                for member in group['members']:
                    member_node = self.add_node('identity', member['email'], 'User')
                    member_node.add_inbound('hierarchy', group_node)
                    group_node.add_outbound('hierarchy', member_node)


class Graph:
    def __init__(self, nodes: List[Node], map: dict):
        self._nodes = nodes
        self._map = map

    def get_node(self, t, id):
        return self._map[(t, id)]

    def get_ancestors(self, node, relation, ancestors=[]):
        inbounds = node.get_inbounds(relation)
        if not inbounds:
            return ancestors
        for inbound in inbounds:
            ancestors.append(inbound)
            self.get_ancestors(inbound, relation, ancestors)

    def get_descendants(self, node, relation, descendants=[]):
        outbounds = node.get_outbounds(relation)
        if not outbounds:
            return descendants
        for outbound in outbounds:
            descendants.append(outbound)
            self.get_descendants(outbound, relation, descendants)


class GCPGraph(Graph):

    def who_has_what(self, who):

        # get user and its groups
        who_asc_nodes = [who]
        self.get_ancestors(who, 'hierarchy', who_asc_nodes)
        # who_asc_nodes += who.get_inbounds('hierarchy')

        result = []

        for node in who_asc_nodes:
            # get roles relations
            relations = [relation for relation in node._relations if relation.startswith('roles/')]
            for relation in relations:
                # get resources
                for res_node in node.get_outbounds(relation):
                    des_res_nodes = [res_node]
                    # get all desc of recource
                    self.get_descendants(res_node, 'hierarchy', des_res_nodes)
                    for des_res_node in des_res_nodes:
                        result.append((des_res_node._id, des_res_node._subtype, relation.split('/')[1]))

        return sorted(list(set(result)), key=lambda k: k[2])

    def what_has_who(self, what):

        # get all parents of resource
        what_asc_nodes = [what]
        self.get_ancestors(what, 'hierarchy', what_asc_nodes)

        result = []

        for node in what_asc_nodes:
            # get roles for resource
            relations = [relation for relation in node._relations if relation.startswith('roles/')]
            for relation in relations:
                # get identites for resouce
                for ident_node in node.get_inbounds(relation):
                    des_ident_nodes = [ident_node]
                    # get ident + groups members
                    self.get_descendants(ident_node, 'hierarchy', des_ident_nodes)
                    for des_ident_node in des_ident_nodes:
                        result.append((des_ident_node._id, relation.split('/')[1]))

        return sorted(list(set(result)), key=lambda k: k[0])
